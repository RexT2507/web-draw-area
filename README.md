# web-draw-area
Plateforme de dessin collaborative

# Installation du projet

## Projet côté server

+ Ouvrez le dossier back
+ Tapez la commande `npm i`
+ Lancer l'API REST en tapant la commande `npm start`
+ export variable environnement (windows): `$env:URL_PREFIX="/api"`

## Projet côté client

+ Ouvrez le dossier front
+ Tapez la commande `npm i`
+ Lancer l'application en tapant la commande `ng serve -o`

# Guide du développement du projet

| Action dans le projet |    Étiquette    |
| :---------------      |:---------------:|
| Ajout                 |      [ADD]      |
| Supprimer             |      [DEL]      |
| Développement         |      [DEV]      |
| Correction            |      [FIX]      |
| Mise à jour de branche|      [MAJ]      |
