import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Component
import { DrawZoneComponent } from './component/draw-zone/draw-zone.component';
import { ToolDashboardComponent } from './component/tool-dashboard/tool-dashboard/tool-dashboard.component';
import { LoginComponent } from './component/login/login.component';
import { ProfilsComponent } from './component/profils/profils.component';
import { RegisterComponent } from './component/register/register.component';
import { PaymentComponent } from './component/payment/payment.component';
import { HomeComponent } from './component/home/home.component';
import { AddsessionComponent } from './component/addsession/addsession.component';
import { SessionComponent } from './component/session/session.component';

//Guard 
import { AuthGuard } from './services/guard/auth.guard';
import { SessionGuard } from './services/guard/session.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'dessin/:idSession',
    canActivate: [AuthGuard, SessionGuard],
    component: ToolDashboardComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'profils',
    canActivate: [AuthGuard],
    component: ProfilsComponent
  },
  {
    path: 'paiement',
    canActivate: [AuthGuard],
    component: PaymentComponent
  },
  { path: 'addsession', 
    canActivate: [AuthGuard],
    component: AddsessionComponent 
  },
  { path: 'session', 
    canActivate: [AuthGuard],
    component: SessionComponent 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
