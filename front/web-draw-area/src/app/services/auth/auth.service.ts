import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ProfilsService } from '../profils/profils.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private URL_API = environment.SOCKET_ENDPOINT + 'api/users/';


  constructor(private http: HttpClient, private router: Router, private profilService : ProfilsService) { }

  register(user: {}) {
    return this.http.post<any>(`${this.URL_API}register`, user);
  }

  login(user: any) {
    return this.http.post<any>(`${this.URL_API}login`, user);
  }

  loggedIn() {
    return !!localStorage.getItem('token');
  }

  getUserID() {
    const token = localStorage.getItem('token');
    const splitToken = token.split('.');
    const tokenSplited = splitToken[1];
    const tokenDecodeB64 = atob(tokenSplited);
    const parseToken = JSON.parse(tokenDecodeB64);
    const tokenSub = parseToken.sub;
    const userId = JSON.parse(tokenSub)._id;
    return userId;
  }

  logoutUser() {
    localStorage.removeItem('token');
    localStorage.removeItem('userEmail');
    localStorage.removeItem('pseudo');
    this.router.navigate(['/home']);
  }

  loginUser(data){
    localStorage.setItem('token', data.token);
    this.profilService.profilsInfo(this.getUserID()).subscribe((data) => {
      localStorage.setItem('userEmail', data.email);
      localStorage.setItem('pseudo', data.pseudo);
      this.router.navigate(['/session']);
    })
  }
  getToken() {
    return localStorage.getItem('token');
  }
}
