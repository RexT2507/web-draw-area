import { Injectable } from '@angular/core';
import { element } from 'protractor';
import { Mouse } from 'src/app/object/Mouse';
import { Tool } from 'src/app/object/Tool';
import { toolCreator } from 'src/app/object/tools/factory/toolCreator';
import { User } from 'src/app/object/User';
import { AuthService } from '../auth/auth.service';
import { SocketService } from '../socket/socket.service';

@Injectable({
  providedIn: 'root'
})
export class ToolService {

  public tool : Tool;
  public mouses: Array<Mouse>;
  public mouse: Mouse;

  private serviceOption: {
    color: string, 
    size: number,
    tolerence: number
  };

  constructor(private socketService: SocketService, private authService: AuthService) {
    this.serviceOption = {
      color: "#000000",
      size: 10,
      tolerence: 50
    }
    this.mouses = [];
    this.tool = toolCreator.createTool('Pencil', this.serviceOption);
    let userActif = new User(this.authService.getUserID(), localStorage.getItem('userEmail'), localStorage.getItem('pseudo'));
    
    this.mouse = new Mouse('Pencil', this.tool.option, "#000000", userActif, this.socketService.session);
  }

  

  set color(color: string){ 
    this.tool.setOption('color', color);
    this.serviceOption.color = color;
    this.mouse.toolOption = this.serviceOption;
    this.socketService.socket.emit('mouse', this.mouse);
  }
  get color(){return this.serviceOption.color}

  set size(size: number){
    let tempoSize: number;
    if(size < 1){
      tempoSize = 1
    }else {
      tempoSize = size
    }
    this.tool.setOption('size', tempoSize);
    this.serviceOption.size = tempoSize;
    this.mouse.toolOption = this.serviceOption;
    this.socketService.socket.emit('mouse', this.mouse);
  }
  get size(){return this.serviceOption.size}

  set tolerence(tolerence: number){ 
    let tempoTolerence: number;
    if(tolerence > 100){
      tempoTolerence = 100
    }else{
      tempoTolerence = tolerence
    }
    this.tool.setOption('tolerence', tempoTolerence);
    this.serviceOption.tolerence = tempoTolerence;
    this.mouse.toolOption = this.serviceOption;
    this.socketService.socket.emit('mouse', this.mouse);
  }
  get tolerence(){return this.serviceOption.tolerence}


  changeTool(toolType){
    this.tool = toolCreator.createTool(toolType, this.serviceOption);
    this.mouse.toolName = toolType;
    this.mouse.toolOption = this.serviceOption;
    this.socketService.socket.emit('mouse', this.mouse)
  }
}
