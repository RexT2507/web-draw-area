import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private URL_API = environment.SOCKET_ENDPOINT + 'api/sessions/'

  constructor(private http: HttpClient) { }

  getAllSession() {
    return this.http.get<any>(`${this.URL_API}session-list`);
  }

  getSessionById(id: string) {
    return this.http.get<any>(`${this.URL_API}session-list/${id}`);
  }

  addSession(session: any) {
    return this.http.post(`${this.URL_API}create`, session);
  }

  updateSession(session: any) {
    return this.http.put(`${this.URL_API}session-list/${session._id}`, session);
  }

  deleteSession(id: string) {
    return this.http.delete(`${this.URL_API}session-list/${id}`);
  }
}
