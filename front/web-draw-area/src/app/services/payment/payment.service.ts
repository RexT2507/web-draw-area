import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  private URL_API = environment.SOCKET_ENDPOINT + "api/payments/";

  constructor(private http: HttpClient, private router: Router) { }

  stripePayment(stripeData: {}) {
    return this.http.post<any>(`${this.URL_API}stripe`, stripeData);
  }
}
