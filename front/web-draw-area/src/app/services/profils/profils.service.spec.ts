import { TestBed } from '@angular/core/testing';

import { ProfilsService } from './profils.service';

import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';


describe('ProfilsService', () => {
  let service: ProfilsService;

  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProfilsService]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ProfilsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
