import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfilsService {

  private URL_PROFILS = environment.SOCKET_ENDPOINT + 'api/users/profils/';

  constructor(private http: HttpClient) { }

  profils() {
    return this.http.get<any>(this.URL_PROFILS);
  }

  profilsInfo(user_id: any) {
    return this.http.get<any>(this.URL_PROFILS + user_id);
  }

  profilsUpdate(user_id: any, fromUpdate: any) {
    return this.http.put<any>(this.URL_PROFILS + user_id, fromUpdate);
  }

  profilsUpdatePass(user_id: any, fromUpdatePass: any) {
    return this.http.put<any>(this.URL_PROFILS + user_id + "/password", fromUpdatePass);
  }

  profilsUpdateRole(user_id: any, fromUpdateRole: string) {
    return this.http.put<any>(this.URL_PROFILS + user_id + "/role", {"role": fromUpdateRole});
  }
}
