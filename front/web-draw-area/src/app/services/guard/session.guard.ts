import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { resourceUsage } from 'process';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { SessionService } from '../session/session.service';

@Injectable({
  providedIn: 'root'
})
export class SessionGuard implements CanActivate {

  constructor(private authService: AuthService, private sessionService: SessionService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    let userId = this.authService.getUserID()
    return this.sessionService.getSessionById(route.paramMap.get('idSession')).pipe(map((res) => {
      if(res.users.find((userIdServeur) => {return userIdServeur == userId}) || res.users.length <= 4){
        return true
      }else{
        this.router.navigate(["/session"])
        return false
      }
    }))
  }
  
}
