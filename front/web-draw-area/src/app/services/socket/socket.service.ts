import { Injectable } from '@angular/core';
import {io, Socket} from 'socket.io-client';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class SocketService {

  socket: Socket;
  session: String;

  constructor() { }

  setupSocketConnection(session, userId) {
    
    this.socket = io(environment.SOCKET_ENDPOINT);
    this.session = session;
    this.socket.on('connect', () => {
      
      this.socket.emit("room", {sessionId: session, userId: userId});
    })
  }

  SocketDisconnect(userId){
    this.socket.emit("disconnectRoom", {sessionId: this.session, userId: userId});
    this.socket.disconnect();
  }
}
