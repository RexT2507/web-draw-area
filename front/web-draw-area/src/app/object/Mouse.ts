import { User } from "./User";

export class Mouse {
    click: boolean;
    move: boolean;
    pos: {x: number, y: number};
    pos_prev: {x: number, y: number};
    user: User;
    userColor: String;
    visible: boolean;
    toolName: String;
    toolOption: Object;
    session: String;

    constructor(toolName: String, toolOption: Object, userColor: String, user: User, session: String, visible = true, click = false, move = false, pos = {x:0, y:0}, pos_prev = {x:0, y:0}) {
        
        this.click = click;
        this.move = move;
        this.pos = pos;
        this.pos_prev = pos_prev;
        this.visible = visible;

        this.toolName = toolName;
        this.toolOption = toolOption;
        this.userColor = userColor;
        this.user = user;
        this.session = session;
    }
}