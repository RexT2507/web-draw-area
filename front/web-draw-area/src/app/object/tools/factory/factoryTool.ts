import { Tool } from "../../Tool";

export abstract class factoryTool {
    abstract operation(option): Tool;
}