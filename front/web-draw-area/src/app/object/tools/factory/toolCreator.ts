import { Tool } from "../../Tool";
import { factoryBrush } from "./factoryBrush";
import { factoryEraser } from "./factoryEraser";
import { factoryPencil } from "./factoryPencil";
import {factoryTool} from "./factoryTool";

export class toolCreator {
    
    static createTool(type: String, option): Tool{
        let constructor: factoryTool;
        let newTool: Tool;

        switch (type) {
            case "Pencil":
                constructor = new factoryPencil();
                newTool = constructor.operation(option);
                return newTool;
            case "Eraser":
                constructor = new factoryEraser();
                newTool = constructor.operation(option);
                return newTool;
            case "Brush":
                constructor = new factoryBrush();
                newTool = constructor.operation(option);
                return newTool;
            default:
                throw "impossible d'instancier l'objet";
        }
    }
}