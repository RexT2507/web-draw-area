import { SocketService } from "src/app/services/socket/socket.service";
import { Tool } from "../../Tool";
import { Brush } from "../Brush";

import { factoryTool } from "./factoryTool";

export class factoryBrush extends factoryTool {

    operation(option): Tool{
        return new Brush(option.color, option.size, option.tolerence)
    }
}
