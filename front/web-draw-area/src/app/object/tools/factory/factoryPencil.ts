import { SocketService } from "src/app/services/socket/socket.service";
import { Tool } from "../../Tool";
import { Pencil } from "../Pencil";
import { factoryTool } from "./factoryTool";

export class factoryPencil extends factoryTool {
    operation(option): Tool{
        return new Pencil(option.color);
    }
}