import { SocketService } from "src/app/services/socket/socket.service";
import { Tool } from "../../Tool";
import { Eraser } from "../Eraser";
import { factoryTool } from "./factoryTool";

export class factoryEraser extends factoryTool {

    operation(option): Tool{
        return new Eraser(option.size)
    }
}