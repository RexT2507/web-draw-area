import { AuthService } from "src/app/services/auth/auth.service";
import { SocketService } from "src/app/services/socket/socket.service";
import { getTextColorByBackground } from "src/app/utils/utils.color";
import { Action } from "../Action";
import { Mouse } from "../Mouse";
import { Tool } from "../Tool";
import { User } from "../User";

export class Eraser implements Tool{
    
    option: {size: number};

    constructor(size: number){
        this.option = {
            size: size
        };
    }
    
    setOption(key: string, value: any): void {
        this.option[key] = value;
    }

    getOption(): string[] {
        return Object.keys(this.option);
    }

    actionLoop(session: String, socketService: SocketService, context: CanvasRenderingContext2D, mouse: Mouse, actionId: number, theDraw: Array<Action>, lastActionId: number): number {
        if (mouse.click && mouse.move && mouse.pos_prev) {
            
            var state = {pos: mouse.pos, pos_prev: mouse.pos_prev, width: context.canvas.clientWidth, height: context.canvas.clientHeight}
            let userTest = new User(1, "jean-didier@gmail.com", "jd13");
            let actionRegister: Action;
            let newActionId: number;
            if(actionId >= 0){
                if(actionId == lastActionId){
                    newActionId = actionId;
                    actionRegister = theDraw[theDraw.findIndex(action => action.idAction == newActionId && action.user == mouse.user._id)];
                }else{
                    return actionId;
                }
            }else {
                newActionId = lastActionId + 1;
                actionRegister = new Action(newActionId, mouse.user._id, 0, session);
            }

            if(!actionRegister){
                return actionId;
            }
            actionRegister.state = [];
            actionRegister.state.push({toolName: 'Eraser', toolOption: this.option, status: state});

            socketService.socket.emit('action', actionRegister);
            this.action(context, state);
            mouse.move = false;
            return newActionId;        
        }
        return -1;
    }

    action(context: CanvasRenderingContext2D, state: {pos: {x: number, y: number}, pos_prev: {x: number, y: number}, width: number, height: number}){
        
        
        var rect = context.canvas.getBoundingClientRect();
        // context.clearRect(state.pos.x - (this._size/2), state.pos.y - (this._size/2), this._size, this._size);
        context.clearRect((state.pos.x * state.width - rect.x) - (this.option.size/2), (state.pos.y * state.height - rect.y)- (this.option.size/2), this.option.size, this.option.size);
    }

    displayCursor(userId: String, mouse: Mouse, width: number, height: number){
        let userTest = new User(1, "jean-didier@gmail.com", "jd13");
        let cursorSize = 16;
        let cursor : string;
        let size = (this.option.size % 2 == 0 ? this.option.size: this.option.size +1);
        cursor = '<span style="pointer-events: none;font-size: ' + String(cursorSize) + 'px;text-shadow: 0 0 3px ' + getTextColorByBackground(mouse.userColor) + ';position:absolute;top:' + String(mouse.pos.y*height-cursorSize - 4) + 'px;left:' + String(mouse.pos.x*width + 4) + 'px;transform:rotate(0.10turn);color:' + mouse.userColor + ';" class="material-icons">crop_portrait</span>'
        +   '<span style="pointer-events: none;width:' + size + 'px;height:' + size + 'px;box-shadow: 0 0 3px #FFFFFF;border:1px solid;border-color:black;position:absolute;opacity:0.5;top:' + String((mouse.pos.y*height)-size/2) + 'px;left:' + String((mouse.pos.x*width)-size/2) + 'px;"></span>';
        
        if(mouse.user._id == userId ){
            cursor = cursor + '<span style="pointer-events: none;font-size: ' + String(cursorSize) + 'px;position:absolute;top:' + String(mouse.pos.y*height-cursorSize/2) + 'px;left:' + String(mouse.pos.x*width-cursorSize/2) + 'px;opacity:0.5;" class="material-icons ">add</span>';
        }else {
            cursor = cursor + '<span style="line-height: 1;text-align: center;pointer-events: none;padding:2px;padding-left:3px;padding-right:3px;box-shadow: 0 0 3px ' + getTextColorByBackground(mouse.userColor) + ';border-radius:' + String(cursorSize/1.8) + 'px;display:table;font-size: ' + String(cursorSize/1.8) + 'px;position:absolute;top:' + String(mouse.pos.y*height-cursorSize - 4 - (cursorSize/2)) + 'px;left:' + String(mouse.pos.x*width + 6 + cursorSize) + 'px;background-color:' + mouse.userColor + ';color:' + getTextColorByBackground(mouse.userColor) + ';">' + mouse.user.pseudo + '</span>';
        }

        return(
            cursor
        ) 
    }
}   