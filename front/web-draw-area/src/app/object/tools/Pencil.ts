import { getMaxListeners } from "process";
import { SocketService } from "src/app/services/socket/socket.service";
import { getTextColorByBackground } from "src/app/utils/utils.color";
import { Action } from "../Action";
import { Mouse } from "../Mouse";
import { Tool } from "../Tool";
import { User } from "../User";

export class Pencil implements Tool{

    option: {color: string};

    constructor(color: string){
        this.option = {
            color: color
        }

    }
    
    

    setOption(key: string, value: any): void {
        this.option[key] = value;
    }
    getOption(): string[] {
        return Object.keys(this.option);
    }
    
    //la fonction return un action Id
    actionLoop(session: String, socketService: SocketService, context: CanvasRenderingContext2D, mouse: Mouse, actionId: number, theDraw: Array<Action>, lastActionId: number): number {
        if (mouse.click && mouse.move && mouse.pos_prev) {
            
            let state = {pos: mouse.pos, pos_prev: mouse.pos_prev, width: context.canvas.clientWidth, height: context.canvas.clientHeight}
            let userTest = new User(1, "jean-didier@gmail.com", "jd13");
            let actionRegister: Action;
            let newActionId: number;
            if(actionId >= 0){
                if(actionId == lastActionId){
                    
                    newActionId = actionId;
                    actionRegister = theDraw[theDraw.findIndex(action => action.idAction == newActionId && action.user == mouse.user._id)];
                }else{
                    return actionId;
                }
            }else {
                newActionId = lastActionId + 1;
                actionRegister = new Action(newActionId, mouse.user._id, 0, session);
            }

            if(!actionRegister){
                return actionId;
            }
            actionRegister.state = [];
            actionRegister.state.push({toolName: 'Pencil', toolOption: this.option, status: state});

            
            socketService.socket.emit('action', actionRegister);
            this.action(context, state);
            
            
            mouse.move = false;
            return newActionId;
        }else if(!mouse.click){
            return -1;
        }

        return actionId
    }

    action(context: CanvasRenderingContext2D, state: {pos: {x: number, y: number}, pos_prev: {x: number, y: number}, width: number, height: number}){
        var rect = context.canvas.getBoundingClientRect();
        
        if(state.pos.y * state.height <= context.canvas.height + rect.y &&  state.pos.x * state.width <= context.canvas.width + rect.x && state.pos.x * state.width >= rect.x && state.pos.y * state.height >= rect.y){
            context.beginPath();
            context.lineWidth = 1;
            context.moveTo(state.pos.x * state.width - rect.x, state.pos.y * state.height - rect.y);
            context.lineTo(state.pos_prev.x * state.width - rect.x, state.pos_prev.y * state.height - rect.y);
            context.strokeStyle = this.option.color;
            context.stroke();
        }
    }

    displayCursor(userId: String, mouse: Mouse, width: number, height: number){
        let cursorSize = 16;
        let cursor : string;
        let size = 2;
        
        cursor = '<span style="pointer-events: none;font-size: ' + String(cursorSize) + 'px;position:absolute;text-shadow: 0 0 3px ' + getTextColorByBackground(mouse.userColor) + ';top:' + String(mouse.pos.y*height-cursorSize - 4) + 'px;left:' + String(mouse.pos.x*width + 4) + 'px;color:' + mouse.userColor + ';" class="material-icons">create</span>'
        +   '<span style="pointer-events: none;width:4px;height:4px;border:1px;border-color:' + getTextColorByBackground(this.option.color) + ';box-shadow: 0 0 3px ' + getTextColorByBackground(this.option.color) + ';;position:absolute;top:' + String(mouse.pos.y*height-cursorSize - 4) + 'px;left:' + String(mouse.pos.x*width + 4) + 'px;background-color: ' + this.option.color + '"></span>'
        +   '<span style="pointer-events: none;width:' + size + 'px;height:' + size + 'px;box-shadow: 0 0 3px #FFFFFF;border:1px solid;border-color:black;border-radius:' + size/2 + 'px;position:absolute;opacity:0.5;top:' + String((mouse.pos.y*height)-size/2) + 'px;left:' + String((mouse.pos.x*width)-size/2) + 'px;"></span>'
       
        
        if(mouse.user._id == userId){
            cursor = cursor + '<span style="pointer-events: none;font-size: ' + String(cursorSize) + 'px;text-shadow: 0 0 3px #FFFFFF;position:absolute;top:' + String(mouse.pos.y*height-cursorSize/2) + 'px;left:' + String(mouse.pos.x*width-cursorSize/2) + 'px;opacity:0.5;" class="material-icons ">add</span>'
        }else {
            cursor = cursor + '<span style="line-height: 1;text-align: center;pointer-events: none;padding:2px;padding-left:3px;padding-right:3px;border-color:' + getTextColorByBackground(mouse.userColor) + ';border-radius:' + String(cursorSize/1.8) + 'px;display:table;font-size: ' + String(cursorSize/1.8) + 'px;position:absolute;top:' + String(mouse.pos.y*height-cursorSize - 4 - (cursorSize/2)) + 'px;left:' + String(mouse.pos.x*width + 6 + cursorSize) + 'px;background-color:' + mouse.userColor + ';color:' + getTextColorByBackground(mouse.userColor) + ';">' + mouse.user.pseudo + '</span>';
        }

        return(
            cursor
        ) 
    }
}