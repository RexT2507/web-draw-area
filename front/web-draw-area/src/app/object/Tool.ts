import { AuthService } from "../services/auth/auth.service";
import { SocketService } from "../services/socket/socket.service";
import { Action } from "./Action";
import { Mouse } from "./Mouse";
import { User } from "./User";

export interface Tool{    
    option: Object;

    actionLoop(session: String, socketService: SocketService, contexte: CanvasRenderingContext2D, mouse: Mouse, actionId: number, theDraw: Array<Action>, lastActionId: number): number;
    action(contexte: CanvasRenderingContext2D, state: {pos: {x: number, y: number}, pos_prev: {x: number, y: number}, width: number, height: number});
    setOption(key: string, value: any): void;
    getOption(): Array<string>;
    displayCursor(userId: String, mouse: Mouse, width: number, height: number);
}