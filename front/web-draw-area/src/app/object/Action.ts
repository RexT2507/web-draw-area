import { User } from "./User";

export class Action {
    _id: string;
    idAction: number;
    state: Array<{
        toolName: string,
        toolOption: Object,
        status: {pos: {x: number, y: number}, pos_prev: {x: number, y: number}, width: number, height: number}
    }>;
    user: String;
    calqueId: number;
    session: String;

    constructor(idAction, user, calqueId, session, id=""){
        this.idAction = idAction;
        this.user = user;
        this.calqueId = calqueId;
        this.state = [];
        this._id = id;
        this.session = session
    }

    addState(state: {toolName: string, toolOption: Object, status: {pos: {x: number, y: number}, pos_prev: {x: number, y: number}, width: number, height: number}}): void{
        this.state.push(state);
    }
}