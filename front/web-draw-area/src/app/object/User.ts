export class User {
    _id: string;
    email: string;
    pseudo: string;

    constructor(id, email, pseudo){
        this._id = id;
        this.email = email;
        this.pseudo = pseudo;
    }
}