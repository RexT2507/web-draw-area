export function brightnessByHexaColor(color: String) {

    const hasFullSpec = color.length == 7;
    let m = color.substr(1).match(hasFullSpec ? /(\S{2})/g : /(\S{1})/g);
    let r: number;
    let g: number;
    let b: number;

    if (m) {
        r = parseInt(m[0] + (hasFullSpec ? '' : m[0]), 16);
        g = parseInt(m[1] + (hasFullSpec ? '' : m[1]), 16);
        b = parseInt(m[2] + (hasFullSpec ? '' : m[2]), 16)
    }

    if (typeof r != "undefined") {
        return ((r * 299) + (g * 587) + (b * 114)) / 1000;
    }
    else {
        return false;
    }
}


export function getTextColorByBackground(HexaColor: String): String{
    let brightness = brightnessByHexaColor(HexaColor);
    if (brightness && brightness > 127.5) {
        return '#000000'
    }else {
        return '#FFFFFF'
    }
}