import { AppComponent } from './app.component';

// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxStripeModule } from 'ngx-stripe';
import { AppRoutingModule } from './app-routing.module';

// Components
import { DrawZoneComponent } from './component/draw-zone/draw-zone.component';
import { ToolDashboardComponent } from './component/tool-dashboard/tool-dashboard/tool-dashboard.component';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { ProfilsComponent } from './component/profils/profils.component';
import { PaymentComponent } from './component/payment/payment.component';


// Services
import { ProfilsService } from './services/profils/profils.service';
import { AuthService } from './services/auth/auth.service';
import { ToolService } from './services/tool/tool.service';
import { SocketService } from './services/socket/socket.service';
import { HeaderComponent } from './component/header/header.component';
import { HomeComponent } from './component/home/home.component';
import { FooterComponent } from './component/footer/footer.component';
import { SessionComponent } from './component/session/session.component';
import { AddsessionComponent } from './component/addsession/addsession.component';

@NgModule({
  declarations: [
    AppComponent,
    DrawZoneComponent,
    ToolDashboardComponent,
    LoginComponent,
    RegisterComponent,
    ProfilsComponent,
    PaymentComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    SessionComponent,
    AddsessionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxStripeModule.forRoot('pk_test_51I7KprHDWRP2I6C0dIGyprUFywvyfOjZpOXt4OECNDY41fxjuKXzVpFpbJpJqphknPmOmmGVyZm27KsU1fCxw99F00RPhiVi6W')
  ],
  providers: [SocketService, ToolService, ProfilsService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
