import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-addsession',
  templateUrl: './addsession.component.html',
  styleUrls: ['./addsession.component.scss']
})
export class AddsessionComponent implements OnInit {

  submitted = false;
  addForm: FormGroup;

  get f() {
    return this.addForm.controls;
  }

  constructor(private sessionService : SessionService, private auth: AuthService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    var userId = this.auth.getUserID();

    this.addForm = this.formBuilder.group({
      _id: [],
      users: userId,
      name: ['', Validators.required]
    });
  }

  onSubmit() {
    this.submitted = true;

    if(this.addForm.valid)
    {
      this.sessionService.addSession(this.addForm.value)
      .subscribe(data => {
        this.router.navigate(['/session']);
      });
    }
  }
}
