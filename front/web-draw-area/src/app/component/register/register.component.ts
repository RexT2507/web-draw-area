import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public email: string;
  public pseudo: string;
  public password: string;
  public role: string;
  public error: string;

  formRegister: {email: string, pseudo: string, password: string, role: string};

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.formRegister = {email: this.email, pseudo: this.pseudo, password: this.password, role: this.role};
    this.error = "";
  }

  submit(){
    this.email = (<HTMLInputElement>document.getElementById("email")).value;
    this.pseudo = (<HTMLInputElement>document.getElementById("pseudo")).value;
    this.password = (<HTMLInputElement>document.getElementById("password")).value;
    this.role = "";

    if(this.email == ""){
      this.error = "Veuillez rentrer une adresse email"
    }else if(this.pseudo == ""){
      this.error = "Veuillez rentrer un pseudo"
    }else if(this.password == ""){
      this.error = "Veuillez rentrer un mot de passe"
    }else{
      this.error = "";
      this.formRegister = {email: this.email, pseudo: this.pseudo, password: this.password, role: this.role};

      this.auth.register(this.formRegister).subscribe(
        data =>{
          this.auth.login({email: this.email, password: this.password, role: this.role}).subscribe(
            data =>{
              this.auth.loginUser(data);
            },
            err => {
              console.log(err);
              this.error = "Adresse mail ou mot de passe incorrect";
            }
          );
        },
        err => console.log(err)
      );
    }
  }
}
