import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxStripeModule, StripeElementsService, StripeService } from 'ngx-stripe';
import { PaymentService } from 'src/app/services/payment/payment.service';

import { PaymentComponent } from './payment.component';

describe('PaymentComponent', () => {
  let component: PaymentComponent;
  let fixture: ComponentFixture<PaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentComponent ],
      imports: [
        HttpClientModule,
        NgxStripeModule,
        RouterTestingModule.withRoutes([])
      ],
      providers: [
        PaymentService,
        {provide: StripeService, useValue: 'pk_test_51I7KprHDWRP2I6C0dIGyprUFywvyfOjZpOXt4OECNDY41fxjuKXzVpFpbJpJqphknPmOmmGVyZm27KsU1fCxw99F00RPhiVi6W'},
        StripeElementsService,
        FormBuilder
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
