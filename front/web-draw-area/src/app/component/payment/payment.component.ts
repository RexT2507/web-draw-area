import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { StripeCardElementOptions, StripeElement, StripeElements, StripeElementsOptions } from '@stripe/stripe-js';
import { StripeCardComponent, StripeService } from 'ngx-stripe';
import { AuthService } from 'src/app/services/auth/auth.service';
import { PaymentService } from 'src/app/services/payment/payment.service';
import { ProfilsService } from 'src/app/services/profils/profils.service';


@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  elements: StripeElements;

  @ViewChild(StripeCardComponent) card: StripeCardComponent;

  paymentStatus: any;

  stripeData: any;

  submitted: any;

  loading: any;

  public name: string;

  cardOptions: StripeCardElementOptions = {
    style: {
      base: {
        iconColor: '#666EE8',
        color: '#31325F',
        fontWeight: '300',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '18px',
        '::placeholder': {
          color: '#4c5b6d'
        }
      }
    }
  };

  elementsOptions: StripeElementsOptions = {
    locale: 'fr'
  };

  stripeForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private stripeService: StripeService,
    private paymentService: PaymentService,
    private authService: AuthService,
    private profilService: ProfilsService,
    private router: Router
  ) {}

  ngOnInit(): void {

    const user = this.authService.getUserID();

    this.profilService.profilsInfo(user).subscribe(
      data => {
        this.name = data.pseudo;
      },
      err => {
        console.log(err);
      }
    );

    this.loading = false;

    this.createForm();

    this.stripeService.elements(this.elementsOptions)
    .subscribe(elements => {

      this.elements = elements;

    });

  }

  createCard() {

  }

  createForm() {
    this.stripeForm = this.fb.group({
      name: ['', [Validators.required]]
    });
  }


  buy(): void {

    const userId = this.authService.getUserID();

    this.submitted = true;

    this.loading = true;

    this.stripeData = this.stripeForm.value;

    this.stripeService
    .createToken(this.card.element, {})
    .subscribe(result => {
      if(result.token) {
        this.stripeData['token'] = result.token
        this.paymentService.stripePayment(this.stripeData).subscribe((res) => {
          if(res['success']) {
            this.loading = false;
            this.submitted = false;
            this.profilService.profilsUpdateRole(userId, "premium").subscribe(data => {
            });
            this.paymentStatus = res['status'];
            this.router.navigate(['/profils']).then(() => {
              window.location.reload();
            });
            console.log(res);
          }
          else {
            this.loading = false;
            this.submitted = false;
            this.paymentStatus = res['status'];
            console.log(res);
          }
        })
      }
      else if(result.error) {
        this.paymentStatus = result.error.message;
      }
    })
  }

}
