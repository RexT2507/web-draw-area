import { Component, OnInit } from '@angular/core';
import { ProfilsService } from '../../services/profils/profils.service';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-profils',
  templateUrl: './profils.component.html',
  styleUrls: ['./profils.component.scss']
})
export class ProfilsComponent implements OnInit {

  public email: string;
  public pseudo: string;
  public password: string;
  public role: string;
  public error: string;
  public user_id: string;

  formUpdate: {email: string, pseudo: string};
  formUpdatePaswword: {password: string};


  constructor(private profils: ProfilsService, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {

    var token = localStorage.getItem('token');

    if(token == null){
      alert("Veuillez vous connecter, pour accéder à cette page");
      this.router.navigate(['/login']);
    }else{
      this.user_id = this.authService.getUserID();

      //Récupération des infos en fonction de l'id de l'utilisateur
      this.email = "";
      this.pseudo = "";
      this.password = "********";
      this.role = "";
      this.error = "";

      this.profils.profilsInfo(this.user_id).subscribe(
        data =>{
          //console.log(data);
          this.email = data.email;
          this.pseudo = data.pseudo;
          this.role = data.role;
        },
        err => {
          console.log(err);
        }
      );

      this.formUpdate = {email: this.email, pseudo: this.pseudo};
    }
  }

  modification(classModif: string){
    if(classModif == "password"){
      (<HTMLInputElement>document.getElementById(classModif)).style.display="none";
      (<HTMLInputElement>document.getElementById(classModif+"Input")).style.display="inline";
      (<HTMLInputElement>document.getElementById(classModif+"Input")).value = "";
      (<HTMLInputElement>document.getElementById(classModif+"Pen")).style.display="none";
      (<HTMLInputElement>document.getElementById(classModif+"Valid")).style.display="inline";
    }else{
      var valeurSpan = (<HTMLInputElement>document.getElementById(classModif)).innerText;
      (<HTMLInputElement>document.getElementById(classModif)).style.display="none";
      (<HTMLInputElement>document.getElementById(classModif+"Input")).style.display="inline";
      (<HTMLInputElement>document.getElementById(classModif+"Input")).value = valeurSpan;
      (<HTMLInputElement>document.getElementById(classModif+"Pen")).style.display="none";
      (<HTMLInputElement>document.getElementById(classModif+"Valid")).style.display="inline";
    }
  }

  validation(classValid: string){
    switch (classValid){
      case "email":
        this.formUpdate ={email: (<HTMLInputElement>document.getElementById(classValid+"Input")).value, pseudo: (<HTMLInputElement>document.getElementById("pseudo")).innerText}
        this.profils.profilsUpdate(this.user_id, this.formUpdate).subscribe(
          data =>{-
            alert("Modification effectué");
          },
          err => {
            console.log(err);
          }
        );

        var valeurInput = (<HTMLInputElement>document.getElementById(classValid+"Input")).value;
        (<HTMLInputElement>document.getElementById(classValid)).style.display="inline";
        (<HTMLInputElement>document.getElementById(classValid+"Input")).style.display="none";
        (<HTMLInputElement>document.getElementById(classValid)).innerText = valeurInput;
        (<HTMLInputElement>document.getElementById(classValid+"Pen")).style.display="inline";
        (<HTMLInputElement>document.getElementById(classValid+"Valid")).style.display="none";
        break;
      case "pseudo":
        this.formUpdate ={email:(<HTMLInputElement>document.getElementById("email")).innerText , pseudo: (<HTMLInputElement>document.getElementById(classValid+"Input")).value}
        this.profils.profilsUpdate(this.user_id, this.formUpdate).subscribe(
          data =>{
            alert("Modification effectué");
          },
          err => {
            console.log(err);
          }
        );

        var valeurInput = (<HTMLInputElement>document.getElementById(classValid+"Input")).value;
        (<HTMLInputElement>document.getElementById(classValid)).style.display="inline";
        (<HTMLInputElement>document.getElementById(classValid+"Input")).style.display="none";
        (<HTMLInputElement>document.getElementById(classValid)).innerText = valeurInput;
        (<HTMLInputElement>document.getElementById(classValid+"Pen")).style.display="inline";
        (<HTMLInputElement>document.getElementById(classValid+"Valid")).style.display="none";
        break;
      case "password":
        this.formUpdatePaswword = {password: (<HTMLInputElement>document.getElementById(classValid+"Input")).value};
        this.profils.profilsUpdatePass(this.user_id, this.formUpdatePaswword).subscribe(
          data =>{
            alert("Modification effectué");
          },
          err => {
            console.log(err);
          }
        );

        (<HTMLInputElement>document.getElementById(classValid)).style.display="inline";
        (<HTMLInputElement>document.getElementById(classValid+"Input")).style.display="none";
        (<HTMLInputElement>document.getElementById(classValid)).innerText = "********";
        (<HTMLInputElement>document.getElementById(classValid+"Pen")).style.display="inline";
        (<HTMLInputElement>document.getElementById(classValid+"Valid")).style.display="none";
        break;
    }
  }
}
