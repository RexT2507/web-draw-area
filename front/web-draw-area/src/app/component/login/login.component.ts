import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public email: string;
  public password: string;
  public error: string;

  formLogin: {email: string, password: string};

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.formLogin = {email: this.email, password: this.password};
    this.error = "";
  }

  submit(){
    this.email = (<HTMLInputElement>document.getElementById("email")).value;
    this.password = (<HTMLInputElement>document.getElementById("password")).value;
  
    if(this.email == ""){
      this.error = "Veuillez rentrer une adresse email"
    }else if(this.password == ""){
      this.error = "Veuillez rentrer un mot de passe"
    }else{
      this.error = "";
      this.formLogin = {email: this.email, password: this.password};

      this.auth.login(this.formLogin).subscribe(
        data =>{
          this.auth.loginUser(data);
        },
        err => {
          console.log(err);
          this.error = "Adresse mail ou mot de passe incorrect";
        }
      );
    }
  }

}
