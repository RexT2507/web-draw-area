import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/object/User';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SocketService } from 'src/app/services/socket/socket.service';
import { ToolService } from 'src/app/services/tool/tool.service';

@Component({
  selector: 'app-tool-dashboard',
  templateUrl: './tool-dashboard.component.html',
  styleUrls: ['./tool-dashboard.component.scss']
})
export class ToolDashboardComponent implements OnInit {

  private routeSub: Subscription;
  private userID: String;
  userTab: Array<User>;

  activeTool: string;
  constructor(private route: ActivatedRoute, public toolService: ToolService,  private authService: AuthService, private socketService: SocketService, private router: Router) {
    this.activeTool = "Pencil";
  }

  ngOnInit(): void {
    this.userID = this.authService.getUserID();
    this.routeSub = this.route.params.subscribe(params => {
      this.socketService.setupSocketConnection(params['idSession'], this.userID);
      this.toolService.mouse.session = params['idSession'];
    });
    this.userTab = [];
    this.socketService.socket.on("userConnect", (user: User) => {
    
      if(user){
        if(!this.userTab.find((userTab) => userTab._id == user._id)){
          this.userTab.push(user);
        }
      }
    })
    this.socketService.socket.on("userDisconnect", (user: User) => {
      if(user){
        let tempoIndex = this.userTab.findIndex((userTab) => userTab._id == user._id);
        if(tempoIndex > -1){
          this.userTab.splice(tempoIndex, 1);
        }
        
        if(user._id == this.userID){
          
          
          this.router.navigate(["/session"])
        }
      }
    })
  }

  ngOnDestroy() {
    this.socketService.SocketDisconnect(this.userID)
    this.routeSub.unsubscribe();
    this.toolService.mouses = []
  }
  

  checkOption(key): boolean{
    return this.toolService.tool.getOption().includes(key)
  }

  changeTool(typeTool: string){
    this.activeTool = typeTool;
    this.toolService.changeTool(typeTool);
    
  }

}
