import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements OnInit {

  constructor(private sessionService : SessionService, private router: Router) { }

  session: any;

  ngOnInit(): void {
    this.getAllSession();
  }

  getAllSession(): void {
    this.sessionService.getAllSession().subscribe(data => {
      this.session = data;
      //console.log(data);
    });
  }

  navigateSession(id: any){
    this.router.navigate(["/dessin", id]);
  }

  delete(idSession: string){
    this.sessionService.deleteSession(idSession).subscribe(data => {
      //console.log(data);
      this.getAllSession();
    });
  }
}