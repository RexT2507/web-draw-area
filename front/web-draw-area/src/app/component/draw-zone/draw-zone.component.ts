import { Component, ElementRef, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Action } from 'src/app/object/Action';
import { Tool } from 'src/app/object/Tool';
import { toolCreator } from 'src/app/object/tools/factory/toolCreator';
import { Pencil } from 'src/app/object/tools/Pencil';
import { SocketService } from 'src/app/services/socket/socket.service';
import { ToolService } from 'src/app/services/tool/tool.service';
import { Mouse } from '../../object/Mouse';
import { User } from 'src/app/object/User';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ProfilsService } from 'src/app/services/profils/profils.service';

@Component({
  selector: 'app-draw-zone',
  templateUrl: './draw-zone.component.html',
  styleUrls: ['./draw-zone.component.scss']
})
export class DrawZoneComponent implements OnInit {
  
  public width: number;
  public height: number;

  public theDraw: Array<Action>;
  public userID: String;
  roleUser: String;
  
  //formulaire resize
  public form: {width: number, height: number};

  lastActionId: number;
  currentAction: number;

  private prevVisibility = false;

  @ViewChild('cursor') 
  zcursor:ElementRef;
  
  @ViewChild('drawing')
  drawing: ElementRef<HTMLCanvasElement>;

  @ViewChild('Filigrane')
  filigrane:ElementRef;

  public context: CanvasRenderingContext2D;
  public contextCursor: any;

  constructor(private router: Router, private socketService: SocketService, public toolService: ToolService, private authService: AuthService, private profilsSevice: ProfilsService) { }

  ngOnInit(): void {
    this.userID = this.authService.getUserID();
    this.profilsSevice.profilsInfo(this.userID ).subscribe((data) => {
      this.roleUser = data.role;
    })
    document.addEventListener("mousedown", this.onClickDown);
    document.addEventListener("mouseup", this.onClickUp);
    document.addEventListener("mousemove", this.onMouseMove);
    
    this.socketService.socket.on('user' + this.userID, (idAction) => {
      
      this.lastActionId = idAction;
    });

    this.width = 1000;
    this.height = 400;
    this.form = {width: this.width, height: this.height};
    this.theDraw = [];
    this.currentAction = -1;
    
  }

  ngOnDestroy() {
    document.removeEventListener("mousedown", this.onClickDown);
    document.removeEventListener("mouseup", this.onClickUp);
    document.removeEventListener("mousemove", this.onMouseMove);
    
  }

  ngAfterViewInit(): void {
    
    this.context = this.drawing.nativeElement.getContext('2d');
    this.contextCursor = this.zcursor.nativeElement;
    
    this.resizeDrawZone(this.width, this.height);

    var rect = this.context.canvas.getBoundingClientRect();
    this.filigrane.nativeElement.style.width = rect.width + "px";
    this.filigrane.nativeElement.style.height = rect.height + "px";
    this.filigrane.nativeElement.style.left = rect.left + "px";
    this.filigrane.nativeElement.style.top = rect.top + "px";
    
    //chargé l'état actuel du dessin
    this.socketService.socket.on('action', (data: Action) => {
      
      if(Object.keys(data).length > 0){
        let findRow = this.theDraw.findIndex(action => action.idAction == data.idAction && action.user == data.user);
        if(findRow >= 0){
          for(var i = 0; i < data.state.length; i++){
            let toolTempo = toolCreator.createTool(data.state[i].toolName, data.state[i].toolOption);
            
            toolTempo.action(this.context, data.state[i].status)
          }
          this.theDraw[findRow].state = this.theDraw[findRow].state.concat(data.state)
        }else {
          let newAction = new Action(data.idAction, data.user, data.calqueId, data.session, data._id);
          
          for(let i = 0; i < data.state.length; i++){
            newAction.addState(data.state[i]);
            let toolTempo = toolCreator.createTool(data.state[i].toolName, data.state[i].toolOption);
            toolTempo.action(this.context, data.state[i].status);
          }
          this.theDraw.push(newAction);
        }
      }
      
    })

    this.socketService.socket.on('mouse', (data: Mouse) => {
      
      let findRow = this.toolService.mouses.findIndex(mouse => mouse.user._id == data.user._id);
      if(findRow >= 0){
        this.toolService.mouses[findRow] = data;
      }else{
        this.toolService.mouses.push(data);
      }
  
    })

    this.contextCursor.innerHTML = '<span id="' + this.toolService.mouse.user._id + '">' 
            + this.toolService.tool.displayCursor(this.userID, this.toolService.mouse, this.width, this.height);
            + '</span>'
    this.bigLoop();
  }

  onClickDown = (event: MouseEvent): void => {
    this.toolService.mouse.click = true;
  }

  onClickUp = (event: MouseEvent): void => {
    this.toolService.mouse.click = false;
  }

  onMouseMove = (event: MouseEvent): void => {
    var rect = this.context.canvas.getBoundingClientRect();
    if(event.clientY <= this.context.canvas.height + rect.y && event.clientX <= this.context.canvas.width + rect.x && event.clientX >= rect.x && event.clientY >= rect.y){
      this.toolService.mouse.pos = {x: event.clientX / this.width, y: event.clientY / this.height};
      this.toolService.mouse.move = true;
      this.toolService.mouse.visible = true;
    }else{
      this.toolService.mouse.move = false;
      this.toolService.mouse.visible = false;
    }
  }
  
  async majCursor(toolTempo: Tool, mouse: Mouse){
    if(this.contextCursor.children[mouse.user._id]){
      if(mouse.visible){
        this.contextCursor.children[mouse.user._id].style.visibility = "visible";
      }else {
        this.contextCursor.children[mouse.user._id].style.visibility = "hidden";
      }
      
      this.contextCursor.children[mouse.user._id].innerHTML = toolTempo.displayCursor(this.userID, mouse, this.width, this.height);
    }
    
  }

  async parcourCursors(){    
    for(let mouse of this.toolService.mouses){
      
      if(this.userID != mouse.user._id){
        if(mouse.pos.x != mouse.pos_prev.x && mouse.pos.y != mouse.pos_prev.y){
          
          let toolTempo = toolCreator.createTool(mouse.toolName, mouse.toolOption);
          if(this.contextCursor.children[mouse.user._id]){
            this.majCursor(toolTempo, mouse);
          }else{
            
            this.contextCursor.innerHTML = this.contextCursor.innerHTML + '<span id="' + mouse.user._id + '">' 
            + toolTempo.displayCursor(this.userID, mouse, this.width, this.height);
            + '</span>'
          }
          
        }
      }
    }
  }

  reDraw(): void{
    // for(var i=0; i < this.theDraw.length; i++){
    //   for(var j=0; j < this.theDraw[i].state.length; j++){
    //     let toolTempo = toolCreator.createTool(this.theDraw[i].state[j].toolName, this.theDraw[i].state[j].toolOption);
    //     toolTempo.action(this.context, this.theDraw[i].state[j].status);
    //   }
      
    // }
  }

  resizeDrawZone(w: number, h: number): void{
    this.width = w;
    this.height = h;
    this.context.canvas.width = w;
    this.context.canvas.height = h;
  }

  submit(){
    this.resizeDrawZone(this.form.width, this.form.height);
    this.reDraw();
  }

  bigLoop(): void{
    this.majCursor(this.toolService.tool, this.toolService.mouse);
    if((this.toolService.mouse.visible || this.prevVisibility) && (this.toolService.mouse.pos.x != this.toolService.mouse.pos_prev.x || this.toolService.mouse.pos.y != this.toolService.mouse.pos_prev.y)){
      
      this.socketService.socket.emit("mouse", this.toolService.mouse);
    }
    this.prevVisibility = this.toolService.mouse.visible

    this.parcourCursors();

    this.currentAction = this.toolService.tool.actionLoop(this.socketService.session, this.socketService, this.context, this.toolService.mouse, this.currentAction, this.theDraw, this.lastActionId);
    
    this.toolService.mouse.pos_prev = {x: this.toolService.mouse.pos.x, y: this.toolService.mouse.pos.y};

    
    if(this.router.url == "/dessin/" + this.socketService.session){
      setTimeout(() => {this.bigLoop()}, 25);
    }
      
  }
}
