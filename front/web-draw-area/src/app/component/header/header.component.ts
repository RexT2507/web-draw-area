import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ProfilsService } from 'src/app/services/profils/profils.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public role: string;

  constructor(public authService: AuthService, private profil: ProfilsService, public router: Router) { }

  ngOnInit(): void {

    const token = localStorage.getItem('token');

    if(token != null)
    {
      const userId = this.authService.getUserID();

      this.profil.profilsInfo(userId).subscribe(
        data => {
          this.role = data.role;
        },
        err => {
          console.log(err);
        }
      );
    }



  }

}
