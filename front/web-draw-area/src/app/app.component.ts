import { Component, HostListener } from '@angular/core';
import { AuthService } from './services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'web-draw-area';
  frameNumber = 0;
  onZone = false;
  pos: {x: number, y: number};
  targetMove: boolean;
  audio: HTMLAudioElement;

  constructor(public authService: AuthService){

    // this.pos={x: 0, y: 0};
    // this.targetMove = false;
    // this.audio = new Audio("../assets/easterEgg/GNEUGNEUGNEU/spidermangneugneu.mp3");
    // this.audio.load();
    // this.audio.loop = true;
    // this.audio.muted = true;
    // this.playCursor();
  }

  mouseout(){
    if(this.onZone){
      this.onZone=false;
    }
  }

  // @HostListener('mousemove', ['$event'])
  // onMouseMove(event: MouseEvent): void{
  //   if((event.target as HTMLTextAreaElement).parentElement.id == "zHeuHeuHeu"){
  //     this.audio.play();
  //     this.onZone=true;
  //   }else {
  //     this.onZone=false;
  //   }
  //   this.pos = {x: event.clientX, y: event.clientY};
  // }

  playCursor(){
    if(this.frameNumber == 11){
      this.frameNumber = 0;
    }else{
      this.frameNumber++
    }

    if(this.onZone){
      this.audio.muted = false;
    }else {
      this.audio.muted = true;
    }
    setTimeout(() => {this.playCursor()}, 90);
  }
}
