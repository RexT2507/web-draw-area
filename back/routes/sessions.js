const express = require('express');
const router = express.Router();

const Session = require('../models/Session');

const mongoose = require('mongoose');

/* GET sessions listing. */
router.get('/', function(req, res, next) {
    res.send(`
    <title>RollTogether API Sessions</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <nav class="navbar navbar-dark" style="background-color: #763045; color: #fff;">     
        <a class="navbar-brand">
        - Router sessions
        </a>
    </nav>
    `);
});


// Création de session
router.post('/create', (req, res) => {

    if(!req.body)
    {
        return res.status(400).send(
            {
                message: "Le contenu d'une session ne peut pas être vide"
            }
        );
    }

    const session = new Session({
        _id: new mongoose.Types.ObjectId(),
        users: [],
        name: req.body.name
    });

    session.save()
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
});


// Liste des sessions
router.get('/session-list', (req, res) => {

    Session.find()
    .then(session => {
        res.send(session);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
});


// Afficher une session via son id
router.get('/session-list/:sessionId', (req, res) => {

    Session.findById(req.params.sessionId)
    .then(session => {

        if(!session)
        {
            return res.status(404).send({
                message: `Aucune session n'a été trouvé avec id : ${req.params.sessionId}`
            });
        }

        res.send(session);
    })
    .catch(err => {

        if(err.kind === 'ObjectId')
        {
            return res.status(404).send({
                message: `Aucune session n'a été trouvé avec id : ${req.params.sessionId}`
            });
        }

        return res.status(500).send({
            message: `Aucun résultat`
        });
    });
});

// Mettre à jour une session
router.put('/session-list/:sessionId', (req, res) => {

    if(!req.body)
    {
        return res.status(400).send({
            message: "La session ne peut pas être vide"
        });
    }

    Session.findByIdAndUpdate(req.params.sessionId, {
        users: req.body.users,
        name: req.body.name
    }, {new: true})
    .then(session => {

        if(!session)
        {
            return res.status(404).send({
                message: `Aucune session n'a été trouvé avec id : ${req.params.sessionId}`
            });
        }
        res.send(session);
    })
    .catch(err => {

        if(err.kind === 'ObjectId')
        {
            return res.status(404).send({
                message: `Aucune session n'a été trouvé avec id : ${req.params.sessionId}`
            });
        }
        return res.status(500).send({
            message: "Un problème est servenu"
        });
    });
});

// Supprimer une session
router.delete('/session-list/:sessionId', (req, res) => {

    Session.findByIdAndRemove(req.params.sessionId)
    .then(session => {

        if(!session)
        {
            return res.status(404).send({
                message: `Aucune session n'a été trouvé avec id : ${req.params.sessionId}`
            });
        }
        res.status(200).send({
            message: "La session a été correctement supprimé"
        });
    })
    .catch(err => {

        if(err.kind === 'ObjectId')
        {
            return res.status(404).send({
                message: `Aucune session n'a été trouvé avec id : ${req.params.sessionId}`
            });
        }
        return res.status(500).send({
            message: "Un problème est survenu"
        });
    });
});

module.exports = router;