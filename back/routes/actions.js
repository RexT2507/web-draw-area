const express = require('express');
const router = express.Router();

const Action = require('../models/Action');

const mongoose = require('mongoose');

/* GET actions listing. */
router.get('/', function(req, res, next) {
    res.send(`
    <title>RollTogether API Sessions</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <nav class="navbar navbar-dark" style="background-color: #763045; color: #fff;">     
        <a class="navbar-brand">
        - Router actions
        </a>
    </nav>
    `);
});

// Créer une action
router.post('/create', (req, res) => {

    if(!req.body)
    {
        return status.res.status(400).send(
            {
                message: "Le contenu d'une action ne peut être vide"
            }
        );
    }

    let tmp = [];

    for(let i = 0; i < req.body.state.length; i++)
    {
        console.log("connard", req.body.state[i]);
        tmp.push({
            toolName: req.body.state[i].toolName,
            toolOption: req.body.state[i].toolOption,
            status: {
                pos: { 
                    x: req.body.state[i].status.pos.x,
                    y: req.body.state[i].status.pos.y
                },
                pos_prev: {
                    x: req.body.state[i].status.pos_prev.x,
                    y: req.body.state[i].status.pos_prev.y
                },
                width: req.body.state[i].status.width,
                height: req.body.state[i].status.height
            }
        });
    }

    const action = new Action({
        _id: new mongoose.Types.ObjectId(),
        idAction: req.body.idAction,
        session: req.body.session,
        state: tmp,
        user: req.body.user,
        calqueId: req.body.calqueId,
        date: Date.now()
    });

    action.save()
    .then(data => {
        console.log(data);
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
});
// Fin de création d'action


// Liste des actions
router.get('/action-list', (req, res) => {
    Action.find()
    .then(action => {
        res.send(action);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
});


// Récupérer une action
router.get('/action-list/:actionId', (req, res) => {

    Action.findById(req.params.actionId)
    .then(action => {

        if(!action)
        {
            return status.res.status(400).send(
                {
                    message: "Le contenu d'une action ne peut être vide"
                }
            );
        }

        res.send(action);
    })
    .catch(err => {

        if(err.kind === 'ObjectId')
        {
            return res.status(404).send({
                message: `Aucune action n'a été trouvé avec id : ${req.params.actionId}`
            });
        }

        return res.status(500).send({
            message: `Aucun résultat`
        });

    });
});

// Mettre à jours une action
router.put('/action-list/:actionId', (req, res) => {

    if(!req.body)
    {
        return status.res.status(400).send(
            {
                message: "Le contenu d'une action ne peut être vide"
            }
        );
    }

    let tmp = [];

    for(let i = 0; i < req.body.state.length; i++)
    {
        console.log("connard", req.body.state[i]);
        tmp.push({
            toolName: req.body.state[i].toolName,
            toolOption: req.body.state[i].toolOption,
            status: {
                pos: { 
                    x: req.body.state[i].status.pos.x,
                    y: req.body.state[i].status.pos.y
                },
                pos_prev: {
                    x: req.body.state[i].status.pos_prev.x,
                    y: req.body.state[i].status.pos_prev.y
                },
                width: req.body.state[i].status.width,
                height: req.body.state[i].status.height
            }
        });
    }

    Action.findByIdAndUpdate(req.params.actionId, {
        
        state: tmp,
        session: req.body.session,
        user: req.body.user,
        calqueId: req.body.calqueId,
        date: Date.now()

    }, {new: true})
    .then(action => {

        if(!action)
        {
            return res.status(404).send({
                message: `Aucune action n'a été trouvé avec id : ${req.params.actionId}`
            });
        }
        res.send(action);
    })
    .catch(err => {

        if(err.kind === 'ObjectId')
        {
            return res.status(404).send({
                message: `Aucune action n'a été trouvé avec id : ${req.params.actionId}`
            });
        }
    });
});


// Supprimer une action
router.delete('/action-list/:actionId', (req, res) => {

    Action.findByIdAndRemove(req.params.actionId)
    .then(action => {

        if(!action)
        {
            return res.status(404).send({
                message: `Aucune action n'a été trouvé avec id : ${req.params.actionId}`
            });
        }
        res.status(200).send({
            message: "L'action a été correctement supprimé"
        });
    })
    .catch(err => {

        if(err.kind === 'ObjectId')
        {
            return res.status(404).send({
                message: `Aucune action n'a été trouvé avec id : ${req.params.actionId}`
            });
        }
        return res.status(500).send({
            message: "Un problème est survenu"
        });
    });
});

// Passe UserId envoit IdAction avec le Max d'IdAction
router.get('/action-list/last-action/:userId', (req, res) => {

    Action.findOne().where({user: req.params.userId}).sort({idAction: -1}).exec(function(err, action) {

        if(err)
        {
            res.send(err);
        }
        res.status(200).send(action);
    });
});

module.exports = router;