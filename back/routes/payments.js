const express = require('express');
const router = express.Router();

const stripe = require('stripe')('sk_test_51I7KprHDWRP2I6C0SlSmYUbtJInqeq497zeM232NUOgO9s8XBKra7IzvoHZ3nRXLqSTzmWLJvIlEVsobcqUm1F2p00UzRt5BeS');

/* GET payment listing. */
router.get('/', function(req, res, next) {
    res.send(`
    <title>RollTogether API Payments</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <nav class="navbar navbar-dark" style="background-color: #763045; color: #fff;">     
        <a class="navbar-brand">
        - Router payments
        </a>
    </nav>
    `);
});

router.post('/stripe', (req, res) => {

    let amount = 500;

    stripe.charges.create({
        amount,
        currency: 'EUR',
        description: 'Abonnement premium',
        source: req.body.token.id
    }, (err, charge) => {

        if(err) 
        {
            return res.status(400).send(err);
        }

        return res.status(200).json({
            success: true,
            status: "Paiement réussi"
        });

    });

    console.log(req.body);
});


module.exports = router;