const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();

const User = require('../models/User');

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');



/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send(`
  <title>RollTogether API Users</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <nav class="navbar navbar-dark" style="background-color: #763045; color: #fff;">     
    <a class="navbar-brand">
      - Router users
    </a>
  </nav>
  `);
});


// Vérification de Token
function verifyToken(req, res, next) 
{
  if (!req.headers.authorization)
  {
    return res.status(401).send('Demande non autorisée');
  }
  let token = req.headers.authorization.split(' ')[1];
  if (token === 'null')
  {
    return res.status(401).send('Demande non autorisée');
  }
  let payload = jwt.verify(token, 'secretKey');
  if (!payload)
  {
    return res.status(401).send('Demande non autorisée');
  }
  req.userId = payload.subject
  next()
}

router.post('/register', (req, res) => {

  bcrypt.hash(req.body.password, 10, (err, hash) => {
    if (err) 
    {
        return res.status(500).json({
            error: err
        });
    }
    else
    {
      const user = new User({
        _id: new mongoose.Types.ObjectId(),
        email: req.body.email,
        pseudo: req.body.pseudo,
        password: hash,
        role: "Utilisateur"
      });    
      
      user.save()
      .then(result => {
        // console.log(result);
        let payload = { subject: user._id };
        let token = jwt.sign(payload, 'secretKey');
        res.status(200).send({
          token,
          message: "Inscription réussite"
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
    }
  })

});// Fin de la méthode register

router.post('/login', (req, res) => 
{
  User.findOne({ email: req.body.email }, function (err, user) {

    if (err)
    {
      return res.status(500).json({
        error: 'Error on the server.'
      });
    }

    if (!user)
    {
      return res.status(404).json({
        error: 'Email invalide'
      });
    }
      
      const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);

      if (!passwordIsValid)
      {
        return res.status(401).json({
          error: 'Mot de passe invalide'
        });
      }
  
  
      const token = jwt.sign({ id: user._id }, 'secretKey', {
        expiresIn: 86400, // expires in 24 hours
        subject: JSON.stringify(user)
      });
      
      res.status(200).send({ auth: true, token: token});
  });

}); // Fin de la méthode login


router.get('/profils', (req, res) => {

  User.find(function(err, user) {
    if(err)
    {
      res.send(err);
    }
    res.json(user);
  })
});


router.get('/profils/:user_id', (req, res) => {

  User.findById(req.params.user_id, function(err, user) {
    if(err)
    {
      res.send(err);
    }
    res.json(user);
  });

});

// Mettre à jour un utilisateur
router.put('/profils/:user_id', (req, res) => {

  if(!req.body)
  {
    return res.status(400).send({
      message: "L'utilisateur ne peut pas être vide"
    });
  }

  User.findByIdAndUpdate(req.params.user_id, {
    pseudo: req.body.pseudo,
    email: req.body.email
  }, {new: true})
  .then(user => {

    if(!user)
    {
      return res.status(404).send({
        message: `Aucun utilisateur trouvé avec cet id : ${req.params.user_id}`
      });
    }
    res.send(user);
  })
  .catch(err => {
    if(err.kind === 'ObjectId')
    {
      return res.status(404).send({
        message: `Aucun planning trouvé avec cet id : ${req.params.user_id}`
      });
    }
    return res.status(500).send({
      message: "Un problème est servenu"
    });
  });
});

// Mettre à jour le rôle
router.put('/profils/:user_id/role', (req, res) => {


  if(!req.body)
  {
    return res.status(400).send({
      message: "L'utilisateur ne peut pas être vide"
    });
  }

  

  User.findByIdAndUpdate(req.params.user_id, {
    role: req.body.role
  }, {new: true})
  .then(user => {

    if(!user)
    {
      return res.status(404).send({
        message: `Aucun utilisateur trouvé avec cet id : ${req.params.user_id}`
      });
    }
    res.send(user);
  })
  .catch(err => {
    if(err.kind === 'ObjectId')
    {
      return res.status(404).send({
        message: `Aucun utilisateur trouvé avec cet id : ${req.params.user_id}`
      });
    }
    return res.status(500).send({
      message: "Un problème est servenu"
    });
  });
  
});

// Mettre à jour le mots de passe
router.put('/profils/:user_id/password', (req, res) => { 

  if(!req.body)
  {
    return res.status(400).send({
      message: "L'utilisateur ne peut pas être vide"
    });
  }

  User.findByIdAndUpdate(req.params.user_id, {
    password: bcrypt.hashSync(req.body.password, 10)
  }, {new: true})
  .then(user => {

    if(!user)
    {
      return res.status(404).send({
        message: `Aucun utilisateur trouvé avec cet id : ${req.params.user_id}`
      });
    }

    res.send(user);
  })
  .catch(err => {
    if(err.kind === 'ObjectId')
    {
      return res.status(404).send({
        message: `Aucun utilisateur trouvé avec cet id : ${req.params.user_id}`
      });
    }
    return res.status(500).send({
      message: "Un problème est servenu"
    });
  });

});

module.exports = router;
