const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const cors = require('cors');

const bodyParser = require('body-parser');

const urlPrefix = process.env.URL_PREFIX;

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const paymentsRouter = require('./routes/payments');
const sessionRouter = require('./routes/sessions');
const actionRouter = require('./routes/actions');

const { cpuUsage } = require('process');

const mongoose = require('mongoose');

const url = 'mongodb://127.0.0.1:27017';
const dbName = 'webDrawAreaDatabase';

const options = {
  useUnifiedTopology: true,
  useNewUrlParser: true,
}

mongoose.connect(`${url}/${dbName}`, options);

const database = mongoose.connection;

database.once('open', _ => {
  console.log(`Connexion MongoDB: ${url}`);
  console.log(`Database: ${dbName}`);
});

database.on('error', err => {
  console.error('Erreur de connexion:', err);
});

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(
  cors()
);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(`${urlPrefix}/`,express.static(path.join(__dirname, 'public')));

// app.use(`${urlPrefix}/`, indexRouter);
app.use(`${urlPrefix}/users`, usersRouter);

app.use(`${urlPrefix}/payments`, paymentsRouter);

app.use(`${urlPrefix}/sessions`, sessionRouter);

app.use(`${urlPrefix}/actions`, actionRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
