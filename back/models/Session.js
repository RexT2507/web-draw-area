const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const sessionSchema = new Schema({

    users: [{
        type: Schema.Types.ObjectId,
        ref: "user"
    }],
    name: String,

});

module.exports = mongoose.model('session', sessionSchema, 'sessions');
