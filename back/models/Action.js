const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const actionSchema = new Schema({

    idAction: {
        type: Number,
        required: true
    },
    session: {
        type: Schema.Types.ObjectId,
        ref: "session"
    },
    state: [{
        toolName: {
            type: String,
            required: true
        },
        toolOption: {
            type: Object,
            required: true
        },
        status: {
            pos: {
                x: {
                    type: Number,
                    required: true
                },
                y: {
                    type: Number,
                    required: true
                }
            },
            pos_prev: {
                x: {
                    type: Number,
                    required: true
                },
                y: {
                    type: Number,
                    required: true
                }
            },
            width: {
                type: Number,
                required: true
            },
            height: {
                type: Number,
                required: true
            }
        }
    }],
    user: {
        type: Schema.Types.ObjectId,
        ref: "user"
    },
    calqueId: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        required: true
    }
});


module.exports = mongoose.model('action', actionSchema, 'actions');